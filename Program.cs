﻿using System;

namespace primer_parcial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            //instanciando los candidatos
            Candidate aspirante1 = new Candidate("Aniceto", "Melendes", "Presidente", "Partdo Social Dominicano", "PSD", 1982);
            Candidate aspirante2 = new Candidate("Matías", "Melendes", "Presidente", "Partdo Corrupto Dominicano", "PCD", 1943);
            Candidate aspirante3 = new Candidate("Adrés", "Melendes", "Presidente", "Partdo Dictatorial Dominicano", "PDicD", 2009);
            Candidate aspirante4 = new Candidate("Hestervino", "Melendes", "Presidente", "Partdo Trujillista Dominicano", "PTD", 2001);



            //************************votación***********************************
            float votosTotales = 0;
            int votosNulos = 0;
            float votantes = 1000;
            while (votosTotales < votantes)
            {   //ver función votar mas abajo
                switch (votar())
                {
                    case 1:
                        aspirante1.votos += 1;
                        votosTotales++;
                        break;

                    case 2:
                        aspirante2.votos += 1;
                        votosTotales++;
                        break;

                    case 3:
                        aspirante3.votos += 1;
                        votosTotales++;
                        break;

                    case 4:
                        aspirante4.votos += 1;
                        votosTotales++;
                        break;

                    default:
                        votosNulos++;
                        break;
                }


            }
            //****************************Mostrar Resultados*******************************
            Candidate[] results = { aspirante1, aspirante2, aspirante3, aspirante4 };

            Console.WriteLine(results[0].votos);
            Array.Sort(results,
                delegate (Candidate x, Candidate y) { return y.votos.CompareTo(x.votos); });


            foreach (var asp in results)
            {
                Console.WriteLine("Candidato: " + asp.nombreCompleto + " ("+asp.siglasPartido +") " + " Votos: " + asp.votos + "   pocentaje: " + Math.Round((asp.votos / votosTotales) * 100, 2) + "%");

            }
            Console.WriteLine("Votos nulos:  " + votosNulos + " para un " + Math.Round((votosNulos / votantes) * 100, 2) + "% del total de votos");

        }

        //acción de votar
        static int votar()
        {
            Random r = new Random();
            int rInt = r.Next(0, Candidate.candidatos + 1);
            return rInt;
        }
    }
    class Candidate
    {
        public string nombre, apellido, nombreCompleto, posicionAspira, partido, siglasPartido;
        public float fundacionPartido, votos;
        public static int candidatos = 0;
        public Candidate(string nombre, string apellido, string posicionAspira, string partido, string siglasPartido, int fundacionPartido)
        {
            this.nombre = nombre;
            this.apellido = apellido;
            this.nombreCompleto = this.nombre + " " + this.apellido;
            this.posicionAspira = posicionAspira;
            this.partido = partido;
            this.fundacionPartido = fundacionPartido;
            this.votos = 0;
            this.siglasPartido = siglasPartido;
            candidatos += 1;
        }

    }
}
